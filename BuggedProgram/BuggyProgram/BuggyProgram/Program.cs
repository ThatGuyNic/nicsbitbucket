﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuggyProgram
{
    class Program
    {
        
        static void Main(string[] args)
        {
            DataModifier data = new DataModifier(5);
            data.Modify(1, 100);
            data.Modify(5, 50);
            Console.WriteLine(data.ReturnValue());
            Console.ReadLine();
        }
    }
}
